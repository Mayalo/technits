﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewProjected.Models
{
    public class Employees
    {
        [Key]
        
        public int EmpID { get; set; }
        [Display(Name ="Tittle")]
        public string title { get; set; }
        [Display(Name ="Employee Name")]
        public string EployeeName { get; set; }
        [Display(Name ="Employee Surname")]
        public string EmpSurname { get; set; }
        public string Gender { get; set; }
        public string Position { get; set; }
        [Display(Name ="Home Adress")]
        public string HomeAdress { get; set; }
        [Display(Name ="Contact number")]
        public string ContactNum { get; set; }
        [Display(Name ="Charge Price")]
        public string EmpCost { get; set; }
    }
    public class Backery
    {
        [Key]
        public int CakeID { get; set; }
        public string Flavour { get; set; }
        public string Colour { get; set; }

    }
    public class comboDeals
    {
        [Key]
        public int ComboID  { get; set; }
        [Display(Name ="Combo Name")]
        public string ComboName { get; set; }
        [Display(Name ="Discount")]
        public double promoDiscount { get; set; }
        [Display(Name ="Total Discount")]
        public double TotalDiscount { get; set; }
    }
    public class Promotions
    {
        public int propID { get; set; }
        public int ResourceID { get; set; }
        [Display(Name ="Start Day")]
        public DateTime StartDate { get; set; }
        [Display(Name ="End Date")]
        public DateTime EndDate { get; set; }

    }
}