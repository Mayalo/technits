﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewProjected.Models
{
    public class Venue
    {
        [Key]
        public int VenueID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        [Display(Name = "Number of Occupants")]
        public int occupantsNumber { get; set; }
        public double Deposit { get; set; }

        [Display(Name = "Cost per day")]
        public double PricePerDay { get; set; }
        [Display(Name = "Picture")]
        public byte[] Pic { get; set; }
    }
    public class Vehicles
    {
        [Key]
        public int VehicleID { get; set; }
        [Required]
        [Display(Name = "Vehicle name")]
        public string CarName { get; set; }
        [Display(Name = "Model")]
        public string Model { get; set; }
        [Display(Name = "Number of seats")]
        public string PassangerNum { get; set; }
        public string Colour { get; set; }
        [Display(Name = "Number Plate")]
        public string numberPlate { get; set; }
        public double Deposit { get; set; }
        [Display(Name = "Price per Day")]
        public double PricePerDay { get; set; }
       
        [Display(Name = "Picture")]
        public byte[] Pic { get; set; }
    }

    public class Tents
    {
        [Key]
        public int TentId { get; set; }
        [Display(Name = "Tent Name")]
        public string TentName { get; set; }
        [Display(Name = "Number of Poles")]
        public string NumPoles { get; set; }
        [Display(Name = "Deposit")]
        public double Deposit { get; set; }
        [Display(Name = "Cost  per day")]
        public double PriceEachDay { get; set; }

        [Display(Name = "Picture")]
        public byte[] Pic { get; set; }
    }
    public class Order
    {
        [Key]
        public int OrderID { get; set; }
        public string UserID { get; set; }
        public bool Status { get; set; }
        [Display(Name = "Total Cost")]
        public double OrderTotal { get; set; }
    }
    public class BookingItems
    {
        [Key]
        public int BookingID { get; set; }
        public int OrderID { get; set; }
        public string UserID { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        [Display(Name = "Pickup Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime PickupDate { get; set; }
        [Display(Name = "Return Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime ReturnDate { get; set; }
        public double Deposit { get; set; }
        [Display(Name = "Rental Price")]
        public double Rental { get; set; }

    }
}
