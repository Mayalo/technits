﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IdentitySample.Models;
using NewProjected.Models;

namespace NewProjected.Controllers
{
    public class TentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Tents
        public ActionResult Index()
        {
            return View(db.Tents.ToList());
        }

        // GET: Tents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tents tents = db.Tents.Find(id);
            if (tents == null)
            {
                return HttpNotFound();
            }
            return View(tents);
        }

        // GET: Tents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "Pic")] Tents tents)
        {
            if (ModelState.IsValid)
            {
                byte[] Pic = null;
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase poImgFile = Request.Files["Pic"];

                    using (var binary = new BinaryReader(poImgFile.InputStream))
                    {
                        Pic = binary.ReadBytes(poImgFile.ContentLength);
                    }
                }
                tents.Pic = Pic;
                tents.Deposit = tents.PriceEachDay / 2;
                db.Tents.Add(tents);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tents);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult NewBooking(BookingItems booking)
        {
            if (ModelState.IsValid)
            {
                if (booking.PickupDate < booking.ReturnDate)
                {
                    var price = db.Tents.Where(l => l.TentId == booking.ItemId && l.TentName == booking.ItemName).Select(p => p.PriceEachDay).FirstOrDefault();
                    if (price==0) 
                    {
                        price = db.Vehicles.Where(l => l.VehicleID == booking.ItemId && l.CarName == booking.ItemName).Select(p => p.PricePerDay).FirstOrDefault();
                    }
                    if (price==0) 
                    {
                        price = db.Venue.Where(l => l.VenueID == booking.ItemId && l.Name == booking.ItemName).Select(p => p.PricePerDay).FirstOrDefault();
                    }

                    booking.Rental = price*(booking.ReturnDate.Day - booking.PickupDate.Day);
                    booking.Deposit = booking.Rental / 2;
                    booking.UserID = User.Identity.Name;
                    db.BookingItems.Add(booking);
                    db.SaveChanges();
                    return RedirectToAction("BookingIndex", "Vehicles");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "The return date cannot be lesser than pick up date !");
                    return View(booking);
                }
            }

            return View(booking);
        }
        [Authorize]
        // GET: Tents/Edit/5
        public ActionResult NewBooking(int itemId,string itemName)
        {
            BookingItems items = new BookingItems();
            items.ItemId = itemId;
            items.ItemName = itemName;
            return View(items);
        }
        
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tents tents = db.Tents.Find(id);
            if (tents == null)
            {
                return HttpNotFound();
            }
            return View(tents);
        }

        // POST: Tents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TentId,TentName,NumPoles,diposit,PriceEachDay,Pic")] Tents tents)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tents).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tents);
        }

        // GET: Tents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tents tents = db.Tents.Find(id);
            if (tents == null)
            {
                return HttpNotFound();
            }
            return View(tents);
        }

        public FileContentResult UserPhotos(int tentId)
        {
            if (tentId == 0)
            {
                string fileName = HttpContext.Server.MapPath(@"~/Images/noImg.png");

                byte[] imageData = null;
                FileInfo fileInfo = new FileInfo(fileName);
                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                imageData = br.ReadBytes((int)imageFileLength);

                return File(imageData, "image/png");

            }
            else
            {
                var pic = db.Tents.Where(k=>k.TentId==tentId).Select(k=> k.Pic).FirstOrDefault();
                return new FileContentResult(pic, "image/jpeg");

            }
        }

        // POST: Tents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tents tents = db.Tents.Find(id);
            db.Tents.Remove(tents);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
