﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IdentitySample.Models;
using NewProjected.Models;

namespace NewProjected.Controllers
{
    public class VehiclesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Vehicles
        public ActionResult Index()
        {
            return View(db.Vehicles.ToList());
        }
        [Authorize]
        public ActionResult BookingIndex()
        {
            return View(db.BookingItems.ToList().Where(l=>l.UserID==User.Identity.Name));
        }
        
        [Authorize]
        public ActionResult BookingItems(int id)
        {
            ViewBag.id = id;
            return View(db.BookingItems.ToList().Where(l=>l.OrderID==id));
        }

        [Authorize]
        public ActionResult OrderDetails()
        { 
            return View(db.Order.Where(l=>l.UserID==User.Identity.Name).ToList());
        }


        [Authorize]
        public ActionResult NewOrder()
        {
            var newOrder = new Order();
            newOrder.UserID = User.Identity.Name;
            newOrder.OrderTotal = db.BookingItems.Where(l => l.UserID == User.Identity.Name).Select(l => l.Rental).Sum();
            db.Order.Add(newOrder);
            db.SaveChanges();

            var bookings = db.BookingItems.Where(o => o.UserID == User.Identity.Name && o.OrderID == 0);
            foreach (var item in bookings)
            {
                item.OrderID = newOrder.OrderID;
                db.Entry(item).State = EntityState.Modified;
            }
            db.SaveChanges();
            return RedirectToAction("OrderDetails");
        }

        // GET: Vehicles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicles vehicles = db.Vehicles.Find(id);
            if (vehicles == null)
            {
                return HttpNotFound();
            }
            return View(vehicles);
        }

        // GET: Vehicles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vehicles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "Pic")]Vehicles vehicles)
        {
            if (ModelState.IsValid)
            {
                byte[] Pic = null;
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase poImgFile = Request.Files["Pic"];

                    using (var binary = new BinaryReader(poImgFile.InputStream))
                    {
                        Pic = binary.ReadBytes(poImgFile.ContentLength);
                    }
                }
                vehicles.Pic = Pic;
                vehicles.Deposit = vehicles.PricePerDay / 2;
                db.Vehicles.Add(vehicles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vehicles);
        }
        public FileContentResult UserPhotos(int vehicleId)
        {
            if (vehicleId == 0)
            {
                string fileName = HttpContext.Server.MapPath(@"~/Images/noImg.png");

                byte[] imageData = null;
                FileInfo fileInfo = new FileInfo(fileName);
                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                imageData = br.ReadBytes((int)imageFileLength);

                return File(imageData, "image/png");

            }
            else
            {
                var pic = db.Vehicles.Where(k => k.VehicleID == vehicleId).Select(k => k.Pic).FirstOrDefault();
                return new FileContentResult(pic, "image/jpeg");

            }
        }

        // GET: Vehicles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicles vehicles = db.Vehicles.Find(id);
            if (vehicles == null)
            {
                return HttpNotFound();
            }
            return View(vehicles);
        }

        // POST: Vehicles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VehicleID,CarName,Model,PassangerNum,Colour,numberPlate,Deposit,PricePerDay,Pic")] Vehicles vehicles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vehicles);
        }

        // GET: Vehicles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicles vehicles = db.Vehicles.Find(id);
            if (vehicles == null)
            {
                return HttpNotFound();
            }
            return View(vehicles);
        }

        // POST: Vehicles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vehicles vehicles = db.Vehicles.Find(id);
            db.Vehicles.Remove(vehicles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
